use hr;
SELECT * FROM departments;
SELECT * FROM employees;

-- cau1: Truy vấn first name, last name, job id và salary của các nhân viên có tên bắt đầu bằng chữ “S”
SELECT first_name, last_name, job_id, salary 
from employees 
where first_name LIKE 'S%';

-- cau2: Viết truy vấn để tìm các nhân viên có số lương (salary) cao nhất
SELECT employee_id, first_name, last_name, job_id, salary 
from employees 
WHERE salary = (select max(salary) from employees);

-- cau3: Viết truy vấn để tìm các nhân viên có số lương lớn thứ hai
	-- Cach 1:
SELECT employee_id, first_name, last_name, job_id, salary 
from employees 
WHERE salary = (SELECT salary 
				from employees 
				order by salary desc 
				limit 2, 1);
      
      -- Cach 2:
SELECT employee_id, first_name, last_name, job_id, salary 
from employees 
WHERE salary = (SELECT MAX(salary) 
				FROM employees 
                WHERE salary < (SELECT MAX(salary) FROM employees ));

-- cau4: Viết truy vấn để tìm các nhân viên có số lương lớn thứ ba
SELECT employee_id, first_name, last_name, job_id, salary 
from employees 
WHERE salary = (SELECT salary 
				from employees 
				order by salary desc 
				limit 3, 1);

-- cau5: Viết truy vấn để hiển thị mức lương của nhân viên cùng với người quản lý tương ứng, tên nhân viên và quản lý kết hợp từ first_name và last_name
select concat(emp.first_name, ' ', emp.last_name) as 'employee', 
		emp.salary as 'emp_sal', 
        concat(man.first_name, ' ', man.last_name) as 'manager', 
        man.salary as 'mgr_sal' 
from employees as man
inner join employees as emp
	on man.employee_id=emp.manager_id;

-- cau6: Viết truy vấn để tìm số lượng nhân viên cần quản lý của mỗi người quản lý, tên quản lý kết hợp từ first_name và last_name.
select sub_query.manager_id as employee_id,
		concat(emp.first_name, ' ', emp.last_name) as manager_name,
        sub_query.number_of_reportees
from (select manager_id, count(employee_id) as number_of_reportees
		from employees
		where manager_id is not null
		group by manager_id) as sub_query
left join employees as emp
	on emp.employee_id = sub_query.manager_id;

-- cau7: Viết truy vấn để tìm được số lượng nhân viên trong mỗi phòng ban sắp xếp theo thứ tự số nhân viên giảm dần
select departments.department_name, v2.emp_count
from (select department_id, count(employee_id) as emp_count
		from employees
		group by department_id) as v2
left join departments
on v2.department_id = departments.department_id
order by v2.emp_count desc;


-- cau8: Viết truy vấn để tìm số lượng nhân viên được thuê trong mỗi năm sắp xếp theo thứ tự số lương nhân viên giảm dần.
select year(hire_date) as hired_year, count(employee_id) as employees_hired_count
from employees
group by year(hire_date)
order by employees_hired_count desc;


-- cau9: Viết truy vấn để lấy mức lượng lớn nhất, nhỏ nhất và mức lương trung bình của các nhân viên (làm tròn mức lương trung bình về số nguyên)
select min(salary) as min_sal, max(salary) as max_sal, round(avg(salary)) as avg_sal
from employees;


-- cau10: Viết truy vấn để chia nhân viên thành ba nhóm dựa vào mức lương, tên nhân viên được kết
select concat(first_name, ' ', last_name) as employee, salary,
		if(salary >= 2000 && salary < 5000, 'low', 
        if (salary >=5000 && salary < 10000 , 'mid', 'high')) as salary_level
from employees
order by employee asc;


-- cau11: Viết truy vấn hiển thị họ tên nhân viên và số điện thoai theo định dạng (_ _ _)-(_ _ _)-(_ _ _ _). Tên nhân viên kết hợp từ first_name và last_name, kết quả hiển thị như bảng dưới đây:
select concat(first_name, ' ', last_name) as employee, 
		replace(phone_number, '.', '-') as phone_number
from employees;


-- cau12: Viết truy vấn để tìm các nhân viên gia nhập vào tháng 08-1994, tên nhân viên kết hợp từ first_name và last_name
select concat(first_name, ' ', last_name) as employee,
		hire_date
from employees
where year(hire_date) = 1994 && month(hire_date) = 8;


-- cau13: Viết truy vấn để tìm những nhân viên có mức lương cao hơn mức lương trung bình của các nhân viên, kết quả sắp xếp theo thứ tự tăng dần của department_id
select concat(employees.first_name, ' ', employees.last_name) as name,
		employees.employee_id,
        departments.department_name as department,
        employees.department_id,
        employees.salary
from employees
left join departments
on employees.department_id = departments.department_id
where employees.salary > (select avg(employees.salary) from employees)
order by employees.department_id asc;


-- cau14: Viết truy vấn để tìm mức lương lớn nhất ở mỗi phòng ban, kết quả sắp xếp theo thứ tự tăng dần của department_id
select employees.department_id,
		departments.department_name as department,
        max(employees.salary)
from employees
left join departments
on employees.department_id=departments.department_id
group by employees.department_id
order by employees.department_id asc;


-- cau15: Viết truy vấn để tìm 5 nhân viên có mức lương thấp nhất
select first_name, last_name, employee_id, salary
from employees
order by salary asc
limit 5;


-- cau16: Viết truy vấn để hiển thị tên nhân viên theo thứ tự ngược lại
select lower(first_name) as name, reverse(lower(first_name)) as name_in_reverse
from employees;


-- cau17: Viết truy vấn để tìm những nhân viên đã gia nhập vào sau ngày 15 của tháng
select employee_id, concat(first_name, ' ', last_name) as employee, hire_date
from employees
where day(hire_date) > 15;


-- cau18: Viết truy vấn để tìm những quản lý và nhân viên làm trong các phòng ban khác nhau, kết quả sắp xếp theo thứ tự tăng dần của tên người quản lý (tên nhân viên và quản lý kết hợp từ first_name và last_name)
select concat(managers.first_name, ' ', managers.last_name) as manager,
		concat(empl.first_name, ' ', empl.last_name) as employee, 
        managers.department_id as mgr_dept, 
		empl.department_id as emp_dept
from employees as empl
inner join employees as managers
on managers.employee_id = empl.manager_id
where managers.department_id <> empl.department_id
order by manager asc;

